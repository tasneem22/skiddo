#!/bin/bash

find skiddo src test -regex '.*\.\(cpp\|hpp\|h\|cc\|cxx\)' -exec clang-format -i {} \;