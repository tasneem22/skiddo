#include <cstring>
#include <iostream>
#include <memory>
#include <skiddo/buffer/buffer.hpp>
#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/main.hpp>
#include <skiddo/parser/ast_builder.hpp>
#include <skiddo/parser/tokenizer.hpp>
#include <skiddo/typechecker/identifier_resolver.hpp>
#include <skiddo/typechecker/type_checker.hpp>
#include <skiddo/utils/utils.hpp>

ProgramConfig parse_args(int argc, char** argv) {
  switch (argc) {
    case 1:
    return {FILE_MODE, "../examples/presentation.skd"}; //used during debugging
      throw FileError("Specify filename");
    case 2:
      if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
        printf(
            "usage: <> [-h] file\n\nSimple skiddo programming language "
            "compiler\n\nPositional arguments\n\tfile\n\nOptional "
            "arguments\n\t-h, --help show this help message and exit\n");
        exit(EXIT_SUCCESS);
      } else {
        return {FILE_MODE, std::string(argv[1])};
      }
      break;
    default:
      throw FileError("Too many arguments");
  }
}

int main([[maybe_unused]] int args, [[maybe_unused]] char** argv) {
  auto cfg = parse_args(args, argv);
  std::shared_ptr<buffer::Buffer> buffer;
  buffer = std::make_shared<buffer::FileBuffer>(cfg.filename);
  parser::Tokenizer tokenizer(buffer.get());
  parser::ASTBuilder ast_builder(&tokenizer);
  sPtr<Program> main_node = ast_builder.parse_program();
  std::vector<sPtr<Program>> nodes;
  for (auto& arg : main_node->body){
    if (dynamic_cast<Import*>(arg.get()) != nullptr) {
      auto import = dynamic_cast<Import*>(arg.get());
      std::cout << "importing " << import->path << std::endl;
      buffer = std::make_shared<buffer::FileBuffer>(import->path);
      parser::Tokenizer tokenizer(buffer.get());
      parser::ASTBuilder ast_builder(&tokenizer);
      sPtr<Program> imported_node = ast_builder.parse_program();
      nodes.push_back(imported_node);
    }
  }
  IdentifierResolver resolver;
  TypeChecker typeChecker;
  Execution execution;
  AstPrinter printer;

  for (auto& node : nodes) {
    node->accept(resolver);
    node->accept(typeChecker);
    // node->accept(printer);
  }

  main_node->accept(resolver);
  // std::cout << "done identifier resolver \n";
  main_node->accept(typeChecker);
  // std::cout << "done type checking  \n";
  main_node->accept(execution);
  // std::cout << "done execution  \n";
  // main_node->accept(printer);
  return 0;
}