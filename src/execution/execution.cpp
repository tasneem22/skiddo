#include <memory>
#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>

using namespace ast;
using namespace std;

void Execution::visit(Program* node) {
  // Check if anything is declared twice in global scope and visit
  // all functions.
  for (auto& body : node->body) {
    body->accept(*this);
  }

  // for (auto const& pair : values) {
  //   cout << "{" << pair.second.b << "\t" << pair.second.i << "\t" << pair.second.d << "\t" << pair.second.l.size()
  //        << "}\n";
  // }
}

void Execution::visit(VariableDecl* node) {
  if (node->value != nullptr) {
    node->value->accept(*this);
    values[node] = tempValue;
  }
}
void Execution::visit(Variable* node) {
  if (node->value != nullptr) {
    node->value->accept(*this);
    values[node] = tempValue;
  }
}
void Execution::visit(ReturnStatement* node) {
  node->returnValue->accept(*this);

  //  throw ReturnException();
}
void Execution::visit(BreakStatement*) { throw WhileBreakException(); }
void Execution::visit(WhileLoop* node) {
  node->condition->accept(*this);
  while (tempValue.b) {
    try {
      node->body->accept(*this);
      node->condition->accept(*this);
    } catch (WhileBreakException&) {
      break;
    }
  }
}

void Execution::visit(Cond* node) {
  node->condition->accept(*this);
  if (tempValue.type != Value::BOOLTYPE) throw RuntimeException("cond expected a boolean type");
  if (tempValue.b) {
    node->ifBody->accept(*this);
  } else {
    node->elseBody->accept(*this);
  }
}

void Execution::visit(LiteralInt* i) {
  tempValue.type = Value::INT;
  tempValue.i = i->value;
}
void Execution::visit(LiteralFloat* d) {
  tempValue.type = Value::DOUBLE;
  tempValue.d = d->value;
}
void Execution::visit(LiteralBool* b) {
  tempValue.type = Value::BOOLTYPE;
  tempValue.b = b->value;
}
void Execution::visit(LiteralString* s) {
  tempValue.type = Value::STRING;
  tempValue.s = s->value;
}
void Execution::visit(FunctionDecl* f) {
  tempValue.type = Value::FUNCTIONTYPE;
  tempValue.f = *f;
}
// todo

void Execution::visit(LiteralList* list) {
  Value temp;
  temp.type = Value::LISTTYPE;
  temp.l = {};
  for (auto& item : list->elements) {
    item->accept(*this);
    temp.l.push_back(tempValue);
  }
  tempValue = temp;
}
void Execution::visit(LiteralNull*) {}

void Execution::visit(Identifier* identifier) { tempValue = values[identifier->declaration.get()]; }

enum ArithmeticType { PLUS, MINUS, MULT, DIV };
enum ComparisonType { EQUAL, NOTEQUAL, GREATER, LESS, GREATEROREQ, LESSOREQ };
enum LogicalType { NOT, AND, OR, XOR };

double arithmetic(const Value& arg1, const Value& arg2, ArithmeticType mathType) {
  double val1 = 0.0;
  double val2 = 0.0;

  if (arg1.type == Value::INT) {
    val1 = (double)arg1.i;
  } else if (arg1.type == Value::DOUBLE) {
    val1 = arg1.d;
  }

  if (arg2.type == Value::INT) {
    val2 = (double)arg2.i;
  } else if (arg2.type == Value::DOUBLE) {
    val2 = arg2.d;
  }

  if (mathType == PLUS) {
    return val1 + val2;
  }
  if (mathType == MINUS) {
    return val1 - val2;
  }
  if (mathType == MULT) {
    return val1 * val2;
  }
  if (mathType == DIV) {
    if (val2 != 0) {
      return val1 / val2;
    } else {
      throw ZeroDivisionError("you can not divide by zero");
    }
  }
  throw RuntimeException("undefined mathematical operator");
}

bool comparison(const Value& first, const Value& second, ComparisonType type) {
  if (first.type == Value ::BOOLTYPE) {
    if (type == EQUAL) {
      return first.b == second.b;
    } else if (type == NOTEQUAL) {
      return first.b != second.b;
    } else if (type == GREATER) {
      return first.b > second.b;
    } else if (type == LESS) {
      return first.b < second.b;
    } else if (type == GREATEROREQ) {
      return first.b >= second.b;
    } else if (type == LESSOREQ) {
      return first.b <= second.b;
    }
  } else if (first.type == Value ::INT) {
    if (type == EQUAL) {
      return first.i == second.i;
    } else if (type == NOTEQUAL) {
      return first.i != second.i;
    } else if (type == GREATER) {
      return first.i > second.i;
    } else if (type == LESS) {
      return first.i < second.i;
    } else if (type == GREATEROREQ) {
      return first.i >= second.i;
    } else if (type == LESSOREQ) {
      return first.i <= second.i;
    }
  } else if (first.type == Value ::DOUBLE) {
    if (type == EQUAL) {
      return first.d == second.d;
    } else if (type == NOTEQUAL) {
      return first.d != second.d;
    } else if (type == GREATER) {
      return first.d > second.d;
    } else if (type == LESS) {
      return first.d < second.d;
    } else if (type == GREATEROREQ) {
      return first.d >= second.d;
    } else if (type == LESSOREQ) {
      return first.d <= second.d;
    }
  } else if (first.type == Value::NILTYPE) {
     if (type == EQUAL) {
      return second.type != Value::NILTYPE;
    } else if (type == NOTEQUAL) {
      return second.type == Value::NILTYPE;
    }
  } 
  throw RuntimeException("undefined comparison operator");
}

bool logical(const Value& first, const Value& second, LogicalType type) {
  if (type == NOT) {
    return !first.b;
  } else if (type == AND) {
    return (first.b && second.b);
  } else if (type == OR) {
    return (first.b || second.b);
  } else if (type == XOR) {
    return (first.b ^ second.b);
  }
  throw RuntimeException("undefined logical operator");
}

void Execution::visit(ast::BuiltInFunctionCall* node) {
  if (node->identifier->name == "plus") {
    vector<Value> numbers;
    for (const auto& arg : node->arguments) {
      arg->accept(*this);
      numbers.push_back(tempValue);
    }
    tempValue.type = Value::DOUBLE;
    tempValue.d = arithmetic(numbers[0], numbers[1], ArithmeticType::PLUS);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "minus") {
    vector<Value> numbers;
    for (const auto& arg : node->arguments) {
      arg->accept(*this);
      numbers.push_back(tempValue);
    }
    tempValue.type = Value::DOUBLE;
    tempValue.d = arithmetic(numbers[0], numbers[1], ArithmeticType::MINUS);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "times") {
    vector<Value> numbers;
    for (auto& arg : node->arguments) {
      arg->accept(*this);
      numbers.push_back(tempValue);
    }
    tempValue.type = Value::DOUBLE;
    tempValue.d = arithmetic(numbers[0], numbers[1], ArithmeticType::MULT);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "divide") {
    vector<Value> numbers;
    for (auto& arg : node->arguments) {
      arg->accept(*this);
      numbers.push_back(tempValue);
    }
    tempValue.type = Value::DOUBLE;
    tempValue.d = arithmetic(numbers[0], numbers[1], ArithmeticType::DIV);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "equal") {
    vector<Value> args;
    for (auto& arg : node->arguments) {
      arg->accept(*this);
      args.push_back(tempValue);
    }
    tempValue.type = Value::BOOLTYPE;
    tempValue.b = comparison(args[0], args[1], ComparisonType::EQUAL);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "nonequal") {
    vector<Value> args;
    for (auto& arg : node->arguments) {
      arg->accept(*this);
      args.push_back(tempValue);
    }
    tempValue.type = Value::BOOLTYPE;
    tempValue.b = comparison(args[0], args[1], ComparisonType::NOTEQUAL);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "less") {
    vector<Value> args;
    for (auto& arg : node->arguments) {
      arg->accept(*this);
      args.push_back(tempValue);
    }
    tempValue.type = Value::BOOLTYPE;
    tempValue.b = comparison(args[0], args[1], ComparisonType::LESS);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "greater") {
    vector<Value> args;
    for (auto& arg : node->arguments) {
      arg->accept(*this);
      args.push_back(tempValue);
    }
    tempValue.type = Value::BOOLTYPE;
    tempValue.b = comparison(args[0], args[1], ComparisonType::GREATER);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "lesseq") {
    vector<Value> args;
    for (auto& arg : node->arguments) {
      arg->accept(*this);
      args.push_back(tempValue);
    }
    tempValue.type = Value::BOOLTYPE;
    tempValue.b = comparison(args[0], args[1], ComparisonType::LESSOREQ);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "greatereq") {
    vector<Value> args;
    for (auto& arg : node->arguments) {
      arg->accept(*this);
      args.push_back(tempValue);
    }
    tempValue.type = Value::BOOLTYPE;
    tempValue.b = comparison(args[0], args[1], ComparisonType::GREATEROREQ);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "not") {
    vector<Value> bools;
    for (auto& arg : node->arguments) {
      arg->accept(*this);
      bools.push_back(tempValue);
    }
    tempValue.type = Value::BOOLTYPE;
    tempValue.b = logical(bools[0], bools[1], LogicalType::NOT);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "and") {
    vector<Value> bools;
    for (auto& arg : node->arguments) {
      arg->accept(*this);
      bools.push_back(tempValue);
    }
    tempValue.type = Value::BOOLTYPE;
    tempValue.b = logical(bools[0], bools[1], LogicalType::AND);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "or") {
    vector<Value> bools;
    for (auto& arg : node->arguments) {
      arg->accept(*this);
      bools.push_back(tempValue);
    }
    tempValue.type = Value::BOOLTYPE;
    tempValue.b = logical(bools[0], bools[1], LogicalType::OR);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "xor") {
    vector<Value> bools;
    for (auto& arg : node->arguments) {
      arg->accept(*this);
      bools.push_back(tempValue);
    }
    tempValue.type = Value::BOOLTYPE;
    tempValue.b = logical(bools[0], bools[1], LogicalType::XOR);
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "head") {
    node->arguments[0]->accept(*this);
    std::vector<Value> elements = tempValue.l;
    if (elements.empty()) {
      tempValue.type = Value::NILTYPE;
      values[node->identifier->declaration.get()] = tempValue;
    } else {
      Value first = elements[0];
      tempValue = first;
      values[node->identifier->declaration.get()] = first;
    }
  } else if (node->identifier->name == "tail") {
    node->arguments[0]->accept(*this);
    std::vector<Value> elements = tempValue.l;
    std::vector<Value> tail = {elements.begin() + 1, elements.end()};
    tempValue.type = Value::LISTTYPE;
    tempValue.l = tail;
    values[node->identifier->declaration.get()] = tempValue;

  } else if (node->identifier->name == "cons") {
    node->arguments[0]->accept(*this);
    Value firstElement = tempValue;
    node->arguments[1]->accept(*this);
    std::vector<Value> secondVal = tempValue.l;

    std::vector<Value> consVect = {firstElement};
    consVect.insert(consVect.end(), secondVal.begin(), secondVal.end());

    tempValue.type = Value::LISTTYPE;
    tempValue.l = consVect;
    values[node->identifier->declaration.get()] = tempValue;
  } else if (node->identifier->name == "print") {
    node->arguments[0]->accept(*this);
    cout << values[node->identifier->declaration.get()] << endl;
    cout << tempValue << endl;
      }
}

void Execution::visit(TypeIdentifier*) {}
void Execution::visit(FunctionCall* node) {
  if (node->identifier->name != "") {
    node->identifier->accept(*this);
  }
  if (node->identifier->declaration == nullptr) {
    throw FatalError("Function " + node->identifier->name +
                     " is not declared before, please declare it before using it");
  }
  
  auto declaration = dynamic_pointer_cast<FunctionDecl>(node->identifier->declaration);

  if (declaration == nullptr) {
    assert(values[node->identifier->declaration.get()].type == Value::FUNCTIONTYPE);
    declaration = make_shared<FunctionDecl>(values[node->identifier->declaration.get()].f);
  }
  auto parameter = declaration->parameters.begin();
  for (auto& arg : node->arguments) {
    arg->accept(*this);
    values[parameter->get()] = tempValue;
    parameter++;
  }

  declaration->body->accept(*this);
  values[node->identifier->declaration.get()] = tempValue;

}

void Execution::visit(Import*) {}
void Execution::visit(IntType*) {}
void Execution::visit(FloatType*) {}
void Execution::visit(BoolType*) {}
void Execution::visit(NullType*) {}

void Execution::visit(Lambda* f) {
  tempValue.type = Value::FUNCTIONTYPE;
  tempValue.f = *f;
}
void Execution::visit(TypeAlias*) {}
void Execution::visit(FunctionType*) {}
void Execution::visit(GenericTypeDecl*) {}
void Execution::visit(ListType*) {}
void Execution::visit(StringType*) {}