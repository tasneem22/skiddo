#include "skiddo/typechecker/type_checker.hpp"

#include <iostream>
#include <skiddo/exceptions/exceptions.hpp>

using namespace ast;
using namespace std;

void TypeChecker::visit(Program* node) {
  // check program body
  for (auto& body : node->body) {
    body->accept(*this);
  }
}

void TypeChecker::visit(FunctionDecl* node) {
  // we don't check it's body here, we check its body after
  // TODO: check that all typeParams.type are null instead of empty
  bool allNull = true, parameterNull = false;
  for (auto& genericType : node->typeParams)
    if (genericType->type != nullptr) allNull = false;

  for (auto& parameter : node->parameters)
    if (parameter->type == nullptr) parameterNull = true;

  if (allNull && !parameterNull) node->body->accept(*this);
}

// todo this part after introducing & entering types
void TypeChecker::visit(TypeAlias*) {}
void TypeChecker::visit(GenericTypeDecl*) {}
void TypeChecker::visit(BuiltInFunctionCall* node) {
  auto size = node->arguments.size();
  for (auto& arg : node->arguments) {
    arg->accept(*this);
    // if (arg->type ==nullptr) {
    //   arg->type = types[arg]
    // }
  }
  // type checking the arithmetic build-in functions
  if (node->identifier->name == "plus" || node->identifier->name == "minus" ||
      node->identifier->name == "times" || node->identifier->name == "divide") {
    if (size != 2) {
      throw SyntaxError(
          "Mathematical Operator Function should have 2 argument!");
    }
    bool type1 = false;
    bool type2 = false;
    bool type3 = false;
    bool type4 = false;
    try {
      type1 = checkSubType(node->arguments[0]->type, make_shared<FloatType>(),
                           "first arg should be of type float");
    } catch (TypeError const&) {
    }
    try {
      type3 = checkSubType(node->arguments[0]->type, make_shared<IntType>(),
                           "first arg should be of type int");
    } catch (TypeError const&) {
    }
    try {
      type2 = checkSubType(node->arguments[1]->type, make_shared<FloatType>(),
                           "second arg should be of type float");
    } catch (TypeError const&) {
    }
    try {
      type4 = checkSubType(node->arguments[1]->type, make_shared<IntType>(),
                           "second arg should be of type int");
    } catch (TypeError const&) {
    }
    // cout << type1 << " " << type3 << " " << type2 << " " << type4 << endl;
    if (!((type1 || type3) && (type2 || type4)))
      throw TypeError("mathematical function args should be of type number");
    auto funcType = make_shared<FunctionType>();
    funcType->returnType = make_shared<FloatType>();
    node->type = funcType;
  }
  // type checking the logical build-in functions
  else if (node->identifier->name == "and" || node->identifier->name == "or" ||
           node->identifier->name == "xor") {
    if (size != 2) {
      throw SyntaxError("Function should have 2 argument!");
    }
    bool type1 = checkSubType(node->arguments[0]->type, make_shared<BoolType>(),
                              "expected bool type");
    bool type2 = checkSubType(node->arguments[1]->type, make_shared<BoolType>(),
                              "expected bool type");

    if (!(type1 && type2)) {
      throw TypeError("Illegal types!");
    }
  } else if (node->identifier->name == "not") {
    if (size != 1) {
      throw SyntaxError(
          "Logical Not Operator Function should have 1 argument!");
    }
    bool type1 = false;
    try {
      type1 = checkSubType(node->arguments[0]->type, make_shared<BoolType>(),
                           "expected bool type");
    } catch (TypeError const&) {
    }
    if (!(type1)) {
      throw TypeError("Illegal types!");
    }
  }
  // type checking the comparison build-in functions
  else if (node->identifier->name == "equal" ||
           node->identifier->name == "nonequal" ||
           node->identifier->name == "greater" ||
           node->identifier->name == "less" ||
           node->identifier->name == "greatereq" ||
           node->identifier->name == "lesseq") {
    node->type = make_shared<BoolType>();
    if (size != 2) {
      throw SyntaxError("Comparison Function should have 2 argument!");
    }

    bool type1 = false;
    bool type2 = false;
    bool type3 = false;
    bool type4 = false;
    bool type5 = false;
    bool type6 = false;

    try {
      type1 = checkSubType(node->arguments[0]->type, make_shared<BoolType>(),
                           "expected bool type");
    } catch (TypeError const&) {
    }
    try {
      type2 = checkSubType(node->arguments[0]->type, make_shared<FloatType>(),
                           "expected float type");
    } catch (TypeError const&) {
    }
    try {
      type5 = checkSubType(node->arguments[0]->type, make_shared<IntType>(),
                           "expected int type");
    } catch (TypeError const&) {
    }
    try {
      type3 = checkSubType(node->arguments[1]->type, make_shared<BoolType>(),
                           "expected bool type");
    } catch (TypeError const&) {
    }
    try {
      type4 = checkSubType(node->arguments[1]->type, make_shared<FloatType>(),
                           "expected float type");
    } catch (TypeError const&) {
    }
    try {
      type6 = checkSubType(node->arguments[1]->type, make_shared<IntType>(),
                           "expected int type");
    } catch (TypeError const&) {
    }
    if (!((type1 && type3) || (type2 && type4) || (type5 && type6))) {
      throw TypeError("Illegal types!");
    }
    auto funcType = make_shared<FunctionType>();
    funcType->returnType = make_shared<BoolType>();
    node->type = funcType;
  } else if (node->identifier->name == "print") {
    if (node->arguments.size() != 1) {
      throw SyntaxError("Print Function should have 1 argument!");
    }
    node->arguments[0]->accept(*this);
  } else if (node->identifier->name == "head" ||
             node->identifier->name == "tail") {
    if (size != 1) {
      throw SyntaxError("List Head and Tail Function should have 1 argument!");
    }
    //    checkSubType(node->arguments[0]->type, make_shared<ListType>(),
    //    "expected list type");
  } else if (node->identifier->name == "cons") {
    auto funcType = make_shared<FunctionType>();
    funcType->returnType = make_shared<ListType>();
    node->type = funcType;
    if (size != 2) {
      throw SyntaxError("List Cons Function should have 2 argument!");
    }
    //    checkSubType(node->arguments[1]->type, make_shared<ListType>(),
    //    "expected list type");
  } else
    throw TypeError("Undefined built-in function");
}

void TypeChecker::visit(BreakStatement*) {}

void TypeChecker::visit(ListType* node) {
  // todo write it after introducing types
  //  if (node->length != nullptr) {
  //    node->length->accept(*this);
  //    // any primitive type can be converted to int
  //  } else if (!m_inFunctionParams && !m_searchArray) {
  //    error(node->begin, "length of array should be always defined");
  //  } // length can be absent in case of routine parameter
  //
  //  node->elementType->accept(*this);
  //  if (m_searchArray) {
  //    m_arrayInnerType = node->elementType;
  //  }
}

void TypeChecker::visit(VariableDecl* node) {
  if (node->type != nullptr) {
    node->type->accept(*this);
    if (node->value != nullptr) {
      node->value->accept(*this);
      checkSubType(node->type, node->value->type,
                   "invalid combination of initial type and initial value");
    }
  } else if (node->value != nullptr) {
    node->value->accept(*this);
    node->type = node->value->type;
  }
}

void TypeChecker::visit(Variable* node) {
  if (node->type != nullptr) {
    node->type->accept(*this);
    if (node->value != nullptr) {
      node->value->accept(*this);
      checkSubType(node->type, node->value->type,
                   "invalid combination of initial type and initial value");
    }
  } else if (node->value != nullptr) {
    node->value->accept(*this);
    node->type = node->value->type;
  }
}

void TypeChecker::visit(ReturnStatement* node) {
  node->returnValue->accept(*this);
}

void TypeChecker::visit(WhileLoop* node) {
  node->condition->accept(*this);
  checkSubType(node->condition->type, make_shared<BoolType>(),
               "expected while condition of type boolean");
  node->body->accept(*this);
}

void TypeChecker::visit(Cond* node) {
  node->condition->accept(*this);
  // check condition is of type boolean
  checkSubType(node->condition->type, make_shared<BoolType>(),
               "expected if condition of type boolean");
  node->ifBody->accept(*this);
  if (node->elseBody != nullptr) {
    node->elseBody->accept(*this);
  }
}

void TypeChecker::visit(Import*) {}
void TypeChecker::visit(Lambda* node) {
  for (auto& parameter : node->parameters) {
    parameter->accept(*this);
    types[parameter.get()] = parameter->type.get();
  }
  node->body->accept(*this);
}
void TypeChecker::visit(LiteralInt*) {}
void TypeChecker::visit(LiteralFloat*) {}
void TypeChecker::visit(LiteralBool*) {}
void TypeChecker::visit(LiteralList*) {}
void TypeChecker::visit(LiteralNull*) {}
void TypeChecker::visit(LiteralString*) {}

void TypeChecker::visit(Identifier* node) {
  // field name is an identifier, but not linked to a variable
  if (node->declaration != nullptr) {
    node->declaration->accept(*this);
    if (node->declaration->getType() != nullptr) {
      node->declaration->getType()->accept(*this);
    }
    node->type = node->declaration->getType();
  }
}

void TypeChecker::visit(FunctionCall* node) {
  if (node->identifier->declaration == nullptr)
    throw TypeError("identifier of function " + node->identifier->name +
                    " has no declaration");
  auto function_decl =
      dynamic_cast<FunctionDecl*>(node->identifier->declaration.get());
  if (function_decl == nullptr)
    throw TypeError(
        "declaration pointer does not point to a function declaration");

  // generics
  if (!function_decl->typeParams.empty()) {
    auto type = node->genericTypeArgs.begin();
    auto& typeParams = function_decl->typeParams;
    for (auto& genericType : typeParams) {
      genericType->type = *type;
      ++type;
    }
  }

  auto parameter = function_decl->parameters.begin();

  for (auto& arg : node->arguments) {
    //    if (function_type_ptr == nullptr) throw TypeError("function argument
    //    count mismatch");
    //
    //    if(function_type_ptr->argType == nullptr)function_type_ptr->argType =
    //    arg->type;
    if (arg->type != nullptr) {
      (*parameter)->type = arg->type;
      types[(*parameter).get()] = arg->type.get();
    }

    if (arg->type != nullptr && (*parameter)->type != nullptr)
      checkSubType((*parameter)->type, arg->type, "invalid argument type");
    //    function_type_ptr =
    //    dynamic_cast<FunctionType*>(function_type_ptr->returnType.get());
    (++parameter);
  }

  // node->identifier->declaration->accept(*this);
  // function_decl->body->accept(*this);

  function_decl->accept(*this);

  auto function_type_ptr =
      dynamic_cast<FunctionType*>(function_decl->getType().get());
  if (function_type_ptr == nullptr)
    throw TypeError("the function should be of type function");

  for (auto& arg : node->arguments) {
    arg->accept(*this);
  }
  // auto argIterator = node->arguments.begin();
  // for(auto& parameter: function_decl->parameters){
  //   if(parameter->type == nullptr)
  //     parameter->type = (*argIterator)->type;
  //   (++argIterator);
  // }

  node->identifier->declaration->accept(*this);
  function_decl->body->accept(*this);
}

void TypeChecker::visit(IntType*) {}
void TypeChecker::visit(FloatType*) {}
void TypeChecker::visit(BoolType*) {}
void TypeChecker::visit(NullType*) {}
void TypeChecker::visit(StringType*) {}

// todo
void TypeChecker::visit(FunctionType*) {}
void TypeChecker::visit(TypeIdentifier*) {}

bool TypeChecker::checkSubType(const sPtr<Type>& superType,
                               const sPtr<Type>& nodeType,
                               const std::string& err) {
                                
  assert(!(superType == nullptr && nodeType == nullptr));
  if (auto alias = dynamic_pointer_cast<TypeIdentifier>(superType);
      alias != nullptr) {
    return checkSubType(alias->declaration, nodeType, err);
  }
  if (auto alias = dynamic_pointer_cast<TypeIdentifier>(nodeType);
      alias != nullptr) {
    return checkSubType(superType, alias->declaration, err);
  }
  if (auto alias = dynamic_pointer_cast<GenericTypeDecl>(superType);
      alias != nullptr) {
    return checkSubType(alias->type, nodeType, err);
  }
  if (auto alias = dynamic_pointer_cast<GenericTypeDecl>(nodeType);
      alias != nullptr) {
    return checkSubType(superType, alias->type, err);
  }

  if (dynamic_pointer_cast<IntType>(superType) != nullptr &&
      dynamic_pointer_cast<IntType>(nodeType) != nullptr) {
    // both nodes are NumberType
    return true;
  } else if (dynamic_pointer_cast<FloatType>(superType) != nullptr &&
             dynamic_pointer_cast<FloatType>(nodeType) != nullptr) {
    // both nodes are NumberType
    return true;

  } else if (dynamic_pointer_cast<BoolType>(superType) != nullptr &&
             dynamic_pointer_cast<BoolType>(nodeType) != nullptr) {
    // both nodes are BoolType
    return true;

  } else if (dynamic_pointer_cast<NullType>(superType) != nullptr &&
             dynamic_pointer_cast<NullType>(nodeType) != nullptr) {
    // both nodes are NullType
    return true;

  } else if (dynamic_pointer_cast<FunctionType>(superType) != nullptr &&
             dynamic_pointer_cast<FunctionType>(nodeType) != nullptr) {
    // both nodes are FunctionType
    return true;

    auto type = dynamic_pointer_cast<FunctionType>(superType);
    auto type2 = dynamic_pointer_cast<FunctionType>(nodeType);
    return checkSubType(type->returnType, type2->returnType, err) &&
           checkSubType(type->argType, type2->argType, err);
  } else if (dynamic_pointer_cast<TypeAlias>(superType) != nullptr &&
             dynamic_pointer_cast<TypeAlias>(nodeType) != nullptr) {
    return true;

  } else if (dynamic_pointer_cast<ListType>(superType) != nullptr &&
             dynamic_pointer_cast<ListType>(nodeType) != nullptr) {
    auto type1 = dynamic_pointer_cast<ListType>(nodeType);
    auto type2 = dynamic_pointer_cast<ListType>(superType);

    return checkSubType(type1->elementType, type2->elementType, err);
  } else if (dynamic_pointer_cast<IntType>(superType) != nullptr &&
             dynamic_pointer_cast<IntType>(nodeType) != nullptr) {
    return true;

  } else {
    throw TypeError(err);
  }
  return true;
}
