#include "skiddo/typechecker/identifier_resolver.hpp"
#include <skiddo/main.hpp>
#include <memory>
#include <skiddo/exceptions/exceptions.hpp>

using namespace ast;

void IdentifierResolver::visit(Program* node) {
  // Check if anything is declared twice in global scope and visit
  // all functions.
  auto oldSize = m_declarations.size();

  for (auto& body : node->body) {
    body->accept(*this);
  }
  m_declarations.resize(oldSize);
}

void IdentifierResolver::visit(FunctionDecl* node) {
  auto shared_pointer = sPtr<FunctionDecl>(node, [] (void*) {});
  m_declarations.push_back(shared_pointer);
  auto oldSizeDeclaration = m_declarations.size();
  auto oldSizeTypes = m_types.size();

  for (auto& genericType : node->typeParams) {
    auto paramName = genericType->name;
    if (std::count_if(node->typeParams.begin(), node->typeParams.end(),
                      [paramName](sPtr<GenericTypeDecl>& typeParam) {
                        return typeParam->name == paramName;
                      }) > 1) {
      throw TypeError("Type parameter '" + paramName +
                      "' is declared twice in function '" + node->name + "'.");
    }
  }

  for (auto& genericType : node->typeParams) {
    m_types.push_back(genericType);
  }

  for (auto& parameter : node->parameters) {
    if (parameter->type != nullptr) {
      parameter->type->accept(*this);
    }
    m_declarations.push_back(parameter);
  }
  if (node->functionType != nullptr) {
    node->functionType->accept(*this);
  }
  node->body->accept(*this);
  m_declarations.resize(oldSizeDeclaration);
  m_types.resize(oldSizeTypes);
}
void IdentifierResolver::visit(Lambda* node) {
  auto shared_pointer = std::make_shared<FunctionDecl>(*node);
  m_declarations.push_back(shared_pointer);
  auto oldSizeDeclaration = m_declarations.size();
  auto oldSizeTypes = m_types.size();

  for (auto& parameter : node->parameters) {
    if (parameter->type != nullptr) {
      parameter->type->accept(*this);
    }
    m_declarations.push_back(parameter);
  }
  if (node->functionType != nullptr) {
    node->functionType->accept(*this);
  }
  node->body->accept(*this);
  m_declarations.resize(oldSizeDeclaration);
  m_types.resize(oldSizeTypes);
}
void IdentifierResolver::visit(TypeAlias* node) {
  if (node->type == nullptr) throw TypeError("undefined alias type");
  node->type->accept(*this);
  auto shared_pointer = std::make_shared<TypeAlias>(*node);
  m_types.push_back(shared_pointer);
}
void IdentifierResolver::visit(GenericTypeDecl* node) {
  auto shared_pointer = std::make_shared<GenericTypeDecl>(*node);
  m_types.push_back(shared_pointer);
}

void IdentifierResolver::visit(FunctionType* node) {
  if (node->argType != nullptr) node->argType->accept(*this);
  if (node->returnType != nullptr) node->returnType->accept(*this);
}

void IdentifierResolver::visit(ListType*) {
  // todo
  //  if (node->length != nullptr) {
  //    node->length->accept(*this);
  //    checkReplacementVar(node->length);
  //  }
  //
  //  node->elementType->accept(*this);
  //  checkReplacementType(node->elementType);
}

void IdentifierResolver::visit(VariableDecl* node) {
  if (node->type != nullptr) {
    node->type->accept(*this);
  }
  if (node->value != nullptr) {
    node->value->accept(*this);
  }
  auto shared_pointer = sPtr<VariableDecl>(node, [] (void*) {});
  m_declarations.push_back(shared_pointer);
}
void IdentifierResolver::visit(Variable* node) {
  if (node->type != nullptr) {
    node->type->accept(*this);
  }
  if (node->value != nullptr) {
    node->value->accept(*this);
  }
  auto previous_decl = findDecl(node->name);
  if (previous_decl == nullptr) {
    throw TypeError("variable " + node->name + " not declared");
  }
  if (dynamic_cast<VariableDecl*>(previous_decl.get()) != nullptr) {
    auto previous_var_decl = dynamic_cast<VariableDecl*>(previous_decl.get());
    previous_var_decl->value = node->value;
  } else if (dynamic_cast<FunctionDecl*>(previous_decl.get()) != nullptr) {
    //  throw TypeError("There is a function of that name");
  } else {
    throw TypeError("Illegal reassigning of variable");
  }
}
void IdentifierResolver::visit(ReturnStatement* node) {
  node->returnValue->accept(*this);
}
void IdentifierResolver::visit(BreakStatement* node) { node->accept(*this); }
void IdentifierResolver::visit(WhileLoop* node) {
  node->condition->accept(*this);
  node->body->accept(*this);
}

void IdentifierResolver::visit(Cond* node) {
  node->condition->accept(*this);
  node->ifBody->accept(*this);
  if (node->elseBody != nullptr) {
    node->elseBody->accept(*this);
  }
}

void IdentifierResolver::visit(LiteralInt*) {}
void IdentifierResolver::visit(BuiltInFunctionCall* node) {
  //  node->identifier->accept(*this);

  for (auto& arg : node->arguments) {
    arg->accept(*this);
  }
}

void IdentifierResolver::visit(LiteralFloat*) {}
void IdentifierResolver::visit(Import*) {}
void IdentifierResolver::visit(LiteralBool*) {}
void IdentifierResolver::visit(LiteralList*) {}
void IdentifierResolver::visit(LiteralNull*) {}
void IdentifierResolver::visit(LiteralString*) {}

void IdentifierResolver::visit(Identifier* node) {
  // Here comes the main work of figuring out which variable/function this
  // identifier refers to
  if (node->declaration != nullptr) {
    return; // this means that the node is lambda and it was declared in-place
  }
  auto declaration = findDecl(node->name);
  if (declaration != nullptr) {
    node->declaration = declaration;
    return;
  }
  throw SyntaxError(
      "identifier " + node->name +
      " is not declared before, please declare it before using it");
}

void IdentifierResolver::visit(TypeIdentifier* node) {
  // Here comes the main work of figuring out which variable/function this
  // identifier refers to
  auto type = findTypeDecl(node->name);
  if (type != nullptr) {
    node->declaration = type;
    return;
  }
  throw SyntaxError(
      "type " + node->name +
      " is not declared before, please declare it before using it");
}

void IdentifierResolver::visit(FunctionCall* node) {
  node->identifier->accept(*this);
  if (node->identifier->declaration != nullptr) {
    node->identifier->declaration->accept(*this);
  }
  auto& typeParams =
      dynamic_cast<FunctionDecl*>(node->identifier->declaration.get())
          ->typeParams;

  // if (typeParams.size() != node->genericTypeArgs.size()) {
  //   throw SyntaxError("Provided types are not same number of types needed");
  // }

  for (auto& arg : node->arguments) {
    arg->accept(*this);
  }
}
void IdentifierResolver::visit(IntType*) {}
void IdentifierResolver::visit(FloatType*) {}
void IdentifierResolver::visit(BoolType*) {}
void IdentifierResolver::visit(NullType*) {}
void IdentifierResolver::visit(StringType*) {}

sPtr<Declaration> IdentifierResolver::findDecl(const std::string& name) {
  for (auto it = m_declarations.rbegin(); it != m_declarations.rend(); it++) {
    auto decl = *it;
    if (decl->name == name) {
      return decl;
    }
  }
  return nullptr;
}

sPtr<TypeDecl> IdentifierResolver::findTypeDecl(const std::string& name) {
  for (auto it = m_types.rbegin(); it != m_types.rend(); it++) {
    auto decl = *it;
    if (decl->name == name) {
      return decl;
    }
  }
  return nullptr;
}
