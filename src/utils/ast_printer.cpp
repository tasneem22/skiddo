#include <string>

#include "skiddo/utils/utils.hpp"

using namespace ast;
using namespace std;

void AstPrinter::visit(LiteralInt *num) {
  stream << " " << to_string(num->value) << " ";
}
void AstPrinter::visit(Import *import) {
  stream << (import->path);
}
void AstPrinter::visit(Lambda *lambda) {
  stream << "lambda";
}
void AstPrinter::visit(LiteralBool *flag) {
  stream << (flag->value ? "true" : "false");
}
void AstPrinter::visit(LiteralList *) {
  stream << "'(";
  //  for (auto elem : list->elements) {
  //    stream << elem << " ";
  //  }
  stream << ")";
}
void AstPrinter::visit(LiteralFloat *decimal) {
  stream << to_string(decimal->value);
}
void AstPrinter::visit(LiteralNull *) { stream << "null"; }
void AstPrinter::visit(LiteralString *node) { stream << node->value; }

void AstPrinter::visit(Cond *cond) {
  stream << "(cond ";
  cond->condition->accept(*this);
  cond->ifBody->accept(*this);
  if (cond->elseBody != nullptr) cond->elseBody->accept(*this);
  stream << ")";
}
void AstPrinter::visit(Program *prog) {
//  stream << "(prog ";
  for (auto &node : prog->body) {
    node->accept(*this);
  }
//  stream << " ) ";
}
void AstPrinter::visit(FunctionCall *funcCall) {
  stream << " ( ";
  stream << funcCall->identifier->name;
  // funcCall->type->accept(*this);
  for (auto &arg : funcCall->arguments) {
    arg->accept(*this);
  }
  stream << " ) ";
}

void AstPrinter::visit(BuiltInFunctionCall *funcCall) {
  stream << " ( ";
  stream << funcCall->identifier->name;
  for (auto &arg : funcCall->arguments) {
    arg->accept(*this);
  }
  stream << " ) ";
}

void AstPrinter::visit(BreakStatement *) { stream << "(break)"; }
void AstPrinter::visit(ReturnStatement *returnStatement) {
  stream << "(return ";
  returnStatement->returnValue->accept(*this);
  stream << ")";
}
void AstPrinter::visit(WhileLoop *whileLoop) {
  stream << "(while ";
  whileLoop->condition->accept(*this);
  whileLoop->body->accept(*this);
  stream << " ) ";
}
void AstPrinter::visit(Identifier *Identifier) {
  stream << Identifier->name;
    if(Identifier->declaration != nullptr)Identifier->declaration->accept(*this);
}

void AstPrinter::visit(VariableDecl *varDecl) {
  if(varDecl->value != nullptr) {
    stream << " ( ";
    varDecl->value->accept(*this);
    stream << " ) ";
  }
}
void AstPrinter::visit(Variable *varDecl) {
  if(varDecl->value != nullptr) {
    stream << " ( ";
    varDecl->value->accept(*this);
    stream << " ) ";
  }
}
void AstPrinter::visit(FunctionDecl *funcDecl) {
  stream << "(func " << funcDecl->name;

  stream << " (";
  for (auto &param : funcDecl->parameters) {
    stream << param->name << " ";
    if (param->type != nullptr) {
      stream << ":";
      param->type->accept(*this);
    }
  }
  stream << ") ";
  if (funcDecl->functionType!= nullptr) {
    if (funcDecl->functionType->returnType!=nullptr) {
      stream << "-> ";
      funcDecl->functionType->returnType->accept(*this);
      stream << " ";
    }
  }

  funcDecl->body->accept(*this);
  stream << ") ";
}

void AstPrinter::visit(FunctionType *function_type) {
  stream << function_type->argType << " " << function_type->returnType;
}
void AstPrinter::visit(GenericTypeDecl *) { stream << "generic_type_node"; }
void AstPrinter::visit(TypeAlias *type_alias) {
  
  if (type_alias->type != nullptr) {
    type_alias->type->accept(*this);
  } else {
    stream << type_alias->name;
  }
}
void AstPrinter::visit(IntType * ){ stream << "int"; }
void AstPrinter::visit(FloatType *) { stream << "float"; }
void AstPrinter::visit(BoolType *) { stream << "bool"; }
void AstPrinter::visit(NullType *) { stream << "null"; }
void AstPrinter::visit(StringType *) { stream << "string"; }
void AstPrinter::visit(ListType *list_type) {
  stream << "list<";
  list_type->elementType->accept(*this);
  stream << ">";
}
void AstPrinter::visit(TypeIdentifier *type_identifier_node) {
  stream << type_identifier_node->name;
}
