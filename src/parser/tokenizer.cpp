#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/parser/tokenizer.hpp>
#include <stdexcept>
#define is_potential_identifier_start(c) ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_')

#define is_potential_identifier_char(c) \
  ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '_' || c == '.')

#define is_potential_number_char(c) (c >= '0' && c <= '9')

using namespace parser;

Tokenizer::Tokenizer(buffer::Buffer* buf) : buf(buf) { this->lineno = 0; }

TokenObject Tokenizer::get() {
  if (!buffer.empty()) {
    auto top = buffer.top();
    buffer.pop();
    return top;
  }
  int c;
  size_t start;
  size_t end;
  while (true) {
    /* Skip spaces and newlines */
    do {
      c = buf.nextc();
    } while (c == ' ' || c == '\t' || c == '\014' || c == '\n' || c == '\r');

    switch (c) {
      case EOF:
        start = buf.cur;
        end = buf.cur;
        return TokenObject{start, end, Token::ENDMARKER, buf.buf->get(start, end)};
      case '#':
        while (c != EOF && c != '\n') c = buf.nextc();
        continue;
      case '/': {
        c = buf.nextc();
        if (c != '#') throw ErrorToken("There must be '#' behind the '/'");
        c = buf.nextc();
        int old_c = 0;
        while (old_c != '#' || c != '/') {
          old_c = c;
          c = buf.nextc();
          if (c == EOF) throw ErrorToken("Unexpected EOF inside multiline comment");
        }
        continue;
      }
      default:
        break;
    }

    break;
  }

  size_t tkn_start = buf.cur - 1;

  switch (c) {
    case ':':
      start = tkn_start;
      end = buf.cur;
      return TokenObject{start, end, Token::COLON, buf.buf->get(start, end)};
    case '<':
      start = tkn_start;
      end = buf.cur;
      return TokenObject{start, end, Token::L_ANG_BRACKET, buf.buf->get(start, end)};
    case '-':
      start = tkn_start;
      c = buf.nextc();
      if (c == '>') {
        end = buf.cur;
        return TokenObject{start, end, Token::ARROW, buf.buf->get(start, end)};
      } else {
        buf.backup(c);
        // throw SyntaxError("");
        break;
      }
    case '>':
      start = tkn_start;
      end = buf.cur;
      return TokenObject{start, end, Token::R_ANG_BRACKET, buf.buf->get(start, end)};
    default:
      break;
  }

  /* Identifier (most frequent token!) */
  if (is_potential_identifier_start(c)) {
    while (is_potential_identifier_char(c)) {
      c = buf.nextc();
    }
    buf.backup(c);
    start = tkn_start;
    end = buf.cur;
    std::string full_token = buf.buf->get(start, end);
    auto pos = keywords_resolution.find(full_token);
    if (pos != keywords_resolution.end()) {
      return TokenObject{start, end, pos->second, buf.buf->get(start, end)};
    }
    return TokenObject{start, end, Token::IDENTIFIER, buf.buf->get(start, end)};
  }

  /* Number */
  if (is_potential_number_char(c) || c == '+' || c == '-') {
    bool found_decimal_separator = false;

    if (c == '+' || c == '-') {
      c = buf.nextc();
      if (!is_potential_number_char(c)) {
        throw SyntaxError("Invalid syntax");
      }
    }

    if (c == '0') {
      c = buf.nextc();
      if (c == '.') {
        found_decimal_separator = true;
        c = buf.nextc();
        if (!is_potential_number_char(c)) {
          throw SyntaxError("Invalid syntax");
        }
      } else if (is_potential_number_char(c)) {
        throw SyntaxError("Leading zeros in decimal literals are not permitted");
      }
    }

    while (is_potential_number_char(c) || c == '.') {
      if (c == '.') {
        if (!found_decimal_separator) {
          c = buf.nextc();
          if (!is_potential_number_char(c)) {
            throw SyntaxError("Invalid syntax");
          }
          found_decimal_separator = true;
        } else {
          throw SyntaxError("Invalid literal");
        }
      }
      c = buf.nextc();
    }

    if (is_potential_identifier_start(c) || c == '-' || c == '+') {
      throw SyntaxError("Invalid literal");
    }

    buf.backup(c);

    start = tkn_start;
    end = buf.cur;
    if (found_decimal_separator) {
      return TokenObject{start, end, Token::FLOAT, buf.buf->get(start, end)};
    }
    return TokenObject{start, end, Token::INTEGER, buf.buf->get(start, end)};
  }

  switch (c) {
    case '\'':
      start = tkn_start;
      end = buf.cur;
      return TokenObject{start, end, Token::QUOTE_SIGN, buf.buf->get(start, end)};
    case '(':
      start = tkn_start;
      end = buf.cur;
      return TokenObject{start, end, Token::LPAR, buf.buf->get(start, end)};
    case ')':
      start = tkn_start;
      end = buf.cur;
      return TokenObject{start, end, Token::RPAR, buf.buf->get(start, end)};
    default:
      break;
  }
  throw SyntaxError("Unknown token");
}

void Tokenizer::unget(const TokenObject& t) { buffer.push(t); }
TokenObject Tokenizer::peek() {
  if (!buffer.empty()) return buffer.top();
  TokenObject token = get();
  buffer.push(token);
  return token;
}
