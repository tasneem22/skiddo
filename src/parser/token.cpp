#include <skiddo/parser/token.hpp>

const char* const parser::TokenNames[] = {
  "ENDMARKER",         // No more tokens
  "IDENTIFIER",        // Identifier (most frequent token!)
  "NILTYPE",               // null
  "BOOLTYPE",              // true or false
  "INTEGER",           // 
  "FLOAT",             // .
  "LPAR",              // (
  "RPAR",              // )
  "QUOTE_SIGN",        // Unevaluated literal
  "COLON",             // :
  "LISTTYPE",             //
  "LAMBDA",           //
  "FUNCTIONTYPE",         //
  "PROG",             //
  "SETQ_FUNCTION",    //
  "RETURN",           //
  "BREAK",            //
  "L_ANG_BRACKET",    //
  "R_ANG_BRACKET",    //
  "ARROW",            //
  "TRUE",             //
  "FALSE",            //
  "INT_TYPE",         //
  "FLOAT_TYPE",       //
  "BOOL_TYPE",        //
  "NULL_TYPE",        //
  "LIST_TYPE",        //
  "TYPE_DEF",
  "BUILT_IN_FUNCTION",
  "WHILE",
  "CONDITION",
  "REASSIGN",
  "IMPORT"
  };
