#include <iostream>
#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/parser/ast_builder.hpp>
#include <string>
#include <typeinfo>
#include <vector>

using parser::Token;
using namespace ast;
using namespace std;

sPtr<ast::Program> ASTBuilder::parse_program() {
  auto prog = make_shared<Program>();
  while (!nextIs(ENDMARKER)) {
    prog->body.push_back(parse_any());
  }
  return prog;
}
sPtr<ast::Program> ASTBuilder::parse_prog() {
  auto prog = make_shared<Program>();
  expect(LPAR);
  expect(PROG);
  while (!nextIs(RPAR)) {
    prog->body.push_back(parse_any());
  }
  expect(RPAR);
  return prog;
}
sPtr<BaseNode> ASTBuilder::parse_any() {
  try {
    return parse_statement();
  } catch (SyntaxError &) {
    return parse_element();
  } catch (TypeDefError &) {
    return parse_type_def();
  }
}
sPtr<Element> ASTBuilder::parse_element() {
  auto next = this->tokenizer->peek();
  switch (next.type) {
    case TYPE_DEF: {
      throw TypeDefError();
    }
    case INTEGER:
      return parse_int();
    case FLOAT:
      return parse_float();
    case NIL:
      return parse_null();
    case TRUE:
    case FALSE:
      return parse_bool();
    case IDENTIFIER:
      return parse_identifier();
    case LPAR: {
      auto lpar = this->tokenizer->get();
      next = this->tokenizer->peek();
      this->tokenizer->unget(lpar);
      switch (next.type) {
        case BUILT_IN_FUNCTION:
          return parse_built_in_function();
        case PROG:
          return parse_prog();
        case CONDITION:
          return parse_cond();
        case IMPORT:
          return parse_import();
        case LAMBDA:
          return parse_lambda();
        case IDENTIFIER:
          return parse_function_call();
        default:
          //IMPORTANT! can break everything if parse_element is called before parse_stataement
          return parse_function_call();
          break;
      }
    }
    case QUOTE_SIGN: {
      auto quoteSign = this->tokenizer->get();
      next = this->tokenizer->peek();
      this->tokenizer->unget(quoteSign);
      switch (next.type) {
        case LPAR:
          return parse_list();
        case IDENTIFIER:
          return parse_string();
        default:
          break;
      }
    }

    default:
      break;
  }

  throw SyntaxError("Expected correct element expression " +
                    std::string(TokenNames[this->tokenizer->peek().type]));
}

sPtr<ast::Statement> ASTBuilder::parse_statement() {
  auto next = this->tokenizer->get();
  auto isNext = this->tokenizer->peek();
  this->tokenizer->unget(next);
  switch (isNext.type) {
    case BREAK:
      return parse_break();
    case RETURN:
      return parse_return();
    case WHILE:
      return parse_while();
    case FUNCTION:
      return parse_function_decl();
    case SETQ_FUNCTION:
      return parse_setq();
    case REASSIGN:
      return parse_reassign();
    default:
      break;
  }
  throw SyntaxError("Expected correct statement, got " +
                    std::string(TokenNames[this->tokenizer->peek().type]));
}
sPtr<Lambda> ASTBuilder::parse_lambda() {
  expect(LPAR);
  expect(LAMBDA);

  auto lambda = make_shared<Lambda>();

  expect(LPAR);
  std::vector<sPtr<VariableDecl>> arguments;
  auto func_type = make_shared<FunctionType>();
  lambda->functionType = func_type;

  while (!nextIs(RPAR)) {
    auto varDecl = make_shared<VariableDecl>();
    auto argument_name = expect(IDENTIFIER);
    varDecl->name = argument_name.value;
    if (nextIs(COLON)) {
      expect(COLON);
      varDecl->type = parse_type();
    }
    arguments.push_back(varDecl);

    func_type->argType = varDecl->type;
    if (!nextIs(RPAR)) {
      auto returnType = make_shared<FunctionType>();
      func_type->returnType = returnType;
      func_type = returnType;
    }
  }

  expect(RPAR);

  if (nextIs(ARROW)) {
    expect(ARROW);
    func_type->returnType = parse_type();
  }

  auto body = parse_any();
  expect(RPAR);
  // lambda->name
  lambda->body = body;
  lambda->parameters = arguments;
  lambda->functionType = func_type;
  return lambda;
}

sPtr<Import> ASTBuilder::parse_import() {
  expect(LPAR);
  expect(IMPORT);
  auto path = expect(IDENTIFIER);
  expect(RPAR);
  return make_shared<Import>(path.value);
}
sPtr<ast::Identifier> ASTBuilder::parse_identifier() {
  auto identifier = expect(IDENTIFIER, "EXPECTED IDENTIFIER");
  return make_shared<Identifier>(identifier.value);
}
sPtr<LiteralInt> ASTBuilder::parse_int() {
  TokenObject integer = expect(INTEGER, "Expected INTEGER");
  auto literal = make_shared<LiteralInt>((int64_t)std::stoll(integer.value));
  return literal;
}

sPtr<LiteralNull> ASTBuilder::parse_null() {
  expect(NIL);
  auto null = make_shared<LiteralNull>();
  null->type = make_shared<NullType>();
  return null;
}
sPtr<LiteralFloat> ASTBuilder::parse_float() {
  TokenObject float_token = expect(FLOAT, "Expected FLOAT");
  auto literal =
      make_shared<LiteralFloat>((double)std::stod(float_token.value));
  literal->type = make_shared<FloatType>();

  return literal;
}
sPtr<LiteralBool> ASTBuilder::parse_bool() {
  auto bool_token = this->tokenizer->get();
  auto literal = make_shared<LiteralBool>(bool_token.type == Token::TRUE);
  literal->type = make_shared<BoolType>();

  return literal;
}
sPtr<LiteralList> ASTBuilder::parse_list() {
  expect(QUOTE_SIGN);
  expect(LPAR);
  std::vector<sPtr<Element>> elements;

  TokenObject next_token;
  while (!nextIs(RPAR)) elements.push_back(parse_element());

  expect(RPAR);
  auto list = make_shared<LiteralList>();
  list->elements = elements;
  auto type = make_shared<ListType>();
  if (!elements.empty()) {
    type->elementType = elements[0]->type;
  }
  list->type = type;
  return list;
}
sPtr<LiteralString> ASTBuilder::parse_string() {
  expect(QUOTE_SIGN);
  TokenObject string_token = expect(IDENTIFIER, "Expected STRING");
  auto literal = make_shared<LiteralString>(string_token.value);
  return literal;
}

sPtr<FunctionCall> ASTBuilder::parse_function_call() {
  expect(LPAR);

  auto identifier = make_shared<Identifier>("");
  if (nextIs(IDENTIFIER)) {
    auto name = expect(IDENTIFIER);
    identifier->name = name.value;
  } else {
    // throw SyntaxError("Expected identifier");
    //TODO, if we pan to pass lambda and other elements for example like below, we gotta assign their declaration
    // at some point
    /*
    (func add (x) (return (lambda (y) (plus x y))))
    ((add 1) 2)
    */
    auto funcDecl = parse_lambda();
    identifier->declaration = funcDecl;
  }
  std::vector<sPtr<Element>> arguments;
  auto call = make_shared<FunctionCall>();

  if (nextIs(L_ANG_BRACKET)) {
    expect(L_ANG_BRACKET);
    while (!nextIs(R_ANG_BRACKET))
      call->genericTypeArgs.push_back(parse_type());
    expect(R_ANG_BRACKET);
  }

  while (!nextIs(RPAR)) {
    arguments.push_back(parse_element());
  }
  expect(RPAR);
  call->identifier = identifier;
  call->arguments = arguments;
  return call;
}

sPtr<BuiltInFunctionCall> ASTBuilder::parse_built_in_function() {
  expect(LPAR);
  auto token = expect(BUILT_IN_FUNCTION);
  auto identifier = make_shared<Identifier>(token.value);
  std::vector<sPtr<Element>> elements;
  while (!nextIs(RPAR)) {
    elements.push_back(parse_element());
  }
  auto function = make_shared<BuiltInFunctionCall>();
  expect(RPAR);
  function->identifier = identifier;
  function->arguments = elements;
  function->identifier->declaration = make_shared<FunctionDecl>();
  return function;
}

sPtr<FunctionDecl> ASTBuilder::parse_function_decl() {
  expect(LPAR);
  expect(FUNCTION);
  auto name = expect(IDENTIFIER);
  auto func_decl = make_shared<FunctionDecl>();

  if (nextIs(L_ANG_BRACKET)) {
    expect(L_ANG_BRACKET);
    while (!nextIs(R_ANG_BRACKET)) {
      auto alias = make_shared<GenericTypeDecl>();
      alias->name = expect(IDENTIFIER).value;
      func_decl->typeParams.push_back(alias);
    }
    expect(R_ANG_BRACKET);
  }

  expect(LPAR);
  std::vector<sPtr<VariableDecl>> arguments;
  auto func_type = make_shared<FunctionType>();

  while (!nextIs(RPAR)) {
    auto varDecl = make_shared<VariableDecl>();
    auto argument_name = expect(IDENTIFIER);
    varDecl->name = argument_name.value;
    if (nextIs(COLON)) {
      expect(COLON);
      varDecl->type = parse_type();
    }
    arguments.push_back(varDecl);
    func_type->argType = varDecl->type;

    // if this is not the last parameter
    if (!nextIs(RPAR)) {
      auto replacementType = make_shared<FunctionType>();
      replacementType->argType = func_type;
      func_type = replacementType;
    }
  }

  expect(RPAR);

  if (nextIs(ARROW)) {
    expect(ARROW);
    func_type->returnType = parse_type();
  }

  auto body = parse_any();
  expect(RPAR);

  func_decl->name = name.value;
  func_decl->parameters = arguments;
  func_decl->functionType = func_type;
  func_decl->body = body;
  return func_decl;
}

sPtr<VariableDecl> ASTBuilder::parse_setq() {
  expect(LPAR);
  expect(SETQ_FUNCTION);
  auto name = expect(IDENTIFIER);
  auto body = parse_element();
  auto varDecl = make_shared<VariableDecl>();
  varDecl->name = name.value;
  varDecl->value = body;
  expect(RPAR);
  return varDecl;
}

sPtr<Variable> ASTBuilder::parse_reassign() {
  expect(LPAR);
  expect(REASSIGN);
  auto name = expect(IDENTIFIER);
  auto body = parse_element();
  auto varDecl = make_shared<Variable>();
  varDecl->name = name.value;
  varDecl->value = body;
  expect(RPAR);
  return varDecl;
}

sPtr<WhileLoop> ASTBuilder::parse_while() {
  expect(LPAR);
  expect(WHILE);
  auto condition = parse_element();
  auto body = parse_any();
  expect(RPAR);
  auto whl = make_shared<WhileLoop>();
  whl->condition = condition;
  whl->body = body;
  return whl;
}

sPtr<Cond> ASTBuilder::parse_cond() {
  expect(LPAR);
  expect(CONDITION);
  auto condition = parse_element();
  auto thenBody = parse_any();
  auto elseBody = parse_any();
  expect(RPAR);
  auto cond = make_shared<Cond>();
  cond->condition = condition;
  cond->ifBody = thenBody;
  cond->elseBody = elseBody;
  return cond;
}
sPtr<ReturnStatement> ASTBuilder::parse_return() {
  expect(LPAR);
  expect(RETURN);
  auto element = parse_element();
  expect(RPAR);
  auto ret = make_shared<ReturnStatement>();
  ret->returnValue = element;
  return ret;
}
sPtr<BreakStatement> ASTBuilder::parse_break() {
  expect(LPAR);
  expect(BREAK);
  expect(RPAR);
  return make_shared<BreakStatement>();
}

sPtr<TypeAlias> ASTBuilder::parse_type_def() {
  expect(LPAR);
  expect(TYPE_DEF);
  auto name = this->tokenizer->get();
  auto type_aliased = parse_type();
  expect(RPAR);
  auto alias = make_shared<TypeAlias>();
  alias->name = name.value;
  alias->type = type_aliased;
  return alias;
}

sPtr<Type> ASTBuilder::parse_type() {
  auto token = this->tokenizer->get();
  switch (token.type) {
    case INT_TYPE:
      return make_shared<IntType>();
    case FLOAT_TYPE:
      return make_shared<FloatType>();
    case BOOL_TYPE:
      return make_shared<BoolType>();
    case LIST_TYPE: {
      auto type = make_shared<ListType>();
      expect(L_ANG_BRACKET);
      auto elementType = parse_type();
      expect(R_ANG_BRACKET);
      type->elementType = elementType;
      return type;
    }
    // case TYPE_IDENTIFIER:
    case IDENTIFIER: {
      /// works with generics, example (func example<T> (param: T) -> T (return
      /// param)) the third T is IDENTIFIER
      auto type = make_shared<TypeIdentifier>();
      type->name = token.value;
      return type;
    }
    default:
      return nullptr;
  }
}

TokenObject ASTBuilder::expect(const Token &tokenType, const std::string &err) {
  auto nextToken = this->tokenizer->get();
  if (nextToken.type != tokenType) {
    throw SyntaxError(err + " Expected correct token " +
                      std::string(TokenNames[tokenType]) + " but received " +
                      std::string(TokenNames[nextToken.type]));
  }
  return nextToken;
}

bool ASTBuilder::nextIs(const Token &tokenType) {
  auto nextToken = this->tokenizer->peek();
  return tokenType == nextToken.type;
}
