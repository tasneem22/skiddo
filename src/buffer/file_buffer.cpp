#include <cstring>
#include <iostream>
#include <skiddo/buffer/file_buffer.hpp>
#include <skiddo/exceptions/exceptions.hpp>
#include <vector>

using buffer::FileBuffer;

FILE* FileBuffer::get_file(const std::string& filename) {
  this->file = fopen(filename.c_str(), "r");
  if (this->file == NULL) {
    throw FileError("Error opening file, supported format is .skd");
  } else {
    return this->file;
  }
}

FileBuffer::FileBuffer(const std::string& filename) { get_file(filename); }

FileBuffer::~FileBuffer() { fclose(this->file); }

bool FileBuffer::read_more() {
  char buffer[4096];
  if (feof(this->file)) return false;
  if (fgets(buffer, 4096, this->file) == NULL) return false;
  this->push(buffer);
  return true;
}
