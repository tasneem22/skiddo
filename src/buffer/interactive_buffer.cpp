#include <readline/history.h>
#include <readline/readline.h>

#include <iostream>
#include <skiddo/buffer/interactive_buffer.hpp>
#include <skiddo/exceptions/exceptions.hpp>
#include <string>

namespace buffer {

bool InteractiveBuffer::read_more() {
  char* raw_line = readline(">>> ");
  if (!raw_line) return false;

  const int len = strlen(raw_line);
  if (len != 0) add_history(raw_line);

  this->push(raw_line, len);
  this->push("\n", 1);
  free(raw_line);

  return true;
}

}  // namespace buffer
