#pragma once

/* Argument must be a char or an int in [-128, 127] or [0, 255]. */
#define Ski_CHARMASK(c) (static_cast<unsigned char>((c)&0xff))
