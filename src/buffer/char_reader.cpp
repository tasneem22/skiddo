#include <cstring>
#include <skiddo/buffer/char_reader.hpp>
#include <stdexcept>

#include "macro.hpp"

using buffer::BufferCharReader;

int BufferCharReader::nextc() {
  while (this->cur >= static_cast<size_t>(buf->inp - buf->buf)) {
    if (!buf->read_more()) return EOF;
  }

  char* cur = buf->buf + (this->cur++);
  return Ski_CHARMASK(*cur);
}

void BufferCharReader::backup(int c) {
  if (c == EOF) return;

  if (this->cur == 0) throw std::runtime_error("BufferCharReader::backup(): tokenizer beginning of buffer");

  char* cur = buf->buf + (--this->cur);
  if (Ski_CHARMASK(*cur) != c) throw std::runtime_error("BufferCharReader::backup(): wrong character");
}
