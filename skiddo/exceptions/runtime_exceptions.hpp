#pragma once

#include <stdexcept>
#include <string>

class RuntimeException : public std::exception {
 private:
  std::string msg;

 public:
  explicit RuntimeException(const std::string& msg) : msg(msg) {}

  const char* what() const throw() { return msg.c_str(); }
};

class FatalError : public std::exception {
 private:
  std::string msg;

 public:
  explicit FatalError(const std::string& msg) : msg("FatalError: " + msg) {}

  const char* what() const throw() { return msg.c_str(); }
};

class KeyboardInterrupt : public RuntimeException {
 public:
  KeyboardInterrupt() : RuntimeException("KeyboardInterrupt") {}
};

class NameError : public RuntimeException {
 public:
  explicit NameError(const std::string& msg) : RuntimeException("NameError: " + msg) {}
};

class TypeError : public RuntimeException {
 public:
  explicit TypeError(const std::string& msg) : RuntimeException("TypeError: " + msg) {}
};

class FileError : public RuntimeException {
 public:
  explicit FileError(const std::string& msg) : RuntimeException("FileError: " + msg) {}
};

class ZeroDivisionError : public RuntimeException {
 public:
  explicit ZeroDivisionError(const std::string& msg) : RuntimeException("ZeroDivisionError: " + msg) {}
};

class ArgsParseError : public RuntimeException {
 public:
  explicit ArgsParseError(const std::string& msg) : RuntimeException("ArgsParseError: " + msg) {}
};
