#pragma once

#include <stdexcept>
#include <string>

class SystemError : public std::exception {};

class WhileBreakException : public SystemError {
  [[nodiscard]] const char* what() const noexcept override { return "WhileBreak Exception"; }
};

class ReturnException : public SystemError {
 private:
  std::string msg;

 public:
  [[nodiscard]] const char* what() const noexcept override { return msg.c_str(); }

};
