#pragma once

#include <algorithm>
#include <iostream>

#include "skiddo/parser/ast.hpp"

/**
 * This visitor performs resolution of variables/function calls to their
 *  declarations.
 * First, it verifies that there are no conflicts in names in each scope.
 * Then, each instance of Identifier is either filled with a reference to its
 *  declaration or replaced with a routine call referring to its function
 *  declaration.
 */
class IdentifierResolver : public ast::Visitor {
 public:
  void visit(ast::Program* Function) override;
  void visit(ast::Import* Function) override;
  void visit(ast::Cond* Function) override;
  void visit(ast::Identifier* Function) override;
  void visit(ast::TypeIdentifier* Function) override;
  void visit(ast::FunctionCall* Function) override;
  void visit(ast::BuiltInFunctionCall* Function) override;

  void visit(ast::LiteralInt* Function) override;
  void visit(ast::LiteralFloat* Function) override;
  void visit(ast::LiteralBool* Function) override;
  void visit(ast::LiteralList* Function) override;
  void visit(ast::LiteralNull* Function) override;
  void visit(ast::LiteralString* Function) override;

  void visit(ast::ReturnStatement* Function) override;
  void visit(ast::BreakStatement* Function) override;
  void visit(ast::WhileLoop* Function) override;

  void visit(ast::FunctionDecl* Function) override;
  void visit(ast::Lambda* Function) override;
  void visit(ast::VariableDecl* Function) override;
  void visit(ast::Variable* Function) override;

  void visit(ast::FunctionType* Function) override;
  void visit(ast::TypeAlias* Function) override;
  void visit(ast::GenericTypeDecl* Function) override;

  void visit(ast::IntType* Function) override;
  void visit(ast::FloatType* Function) override;
  void visit(ast::BoolType* Function) override;
  void visit(ast::ListType* Function) override;
  void visit(ast::NullType* Function) override;
  void visit(ast::StringType* Function) override;

 private:
  std::vector<ast::sPtr<ast::Declaration>> m_declarations;
  std::vector<ast::sPtr<ast::TypeDecl>> m_types;
  ast::sPtr<ast::Declaration> findDecl(const std::string&);
  ast::sPtr<ast::TypeDecl> findTypeDecl(const std::string&);
};