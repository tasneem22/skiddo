#pragma once

#include <iostream>
#include <map>
#include <memory>

#include "cstring"
#include "skiddo/parser/ast.hpp"
using namespace ast;
struct Value {
  enum { STRING, INT, DOUBLE, BOOLTYPE, FUNCTIONTYPE, NILTYPE, LISTTYPE } type;
  // union {
    std::string s;
    int i{};
    double d;
    bool b;
    std::vector<Value> l;

    ast::FunctionDecl f;
  // };

  ~Value() {
    switch (type) {
      case STRING:
        s.~basic_string();
        break;
      case FUNCTIONTYPE:
        // TODO
        break;
      default:
        break;
    }
  }
  Value(){
    // memset(this, 0, sizeof(*this));
  }
  Value(Value const &other) {
    *this = other;
  }

  Value& operator=(const Value& other) {
    type = other.type;
    switch (type) {
      case STRING:
        new (&s) auto(other.s);
        break;
      case INT:
        i = other.i;
        break;
      case DOUBLE:
        d = other.d;
        break;
      case BOOLTYPE:
        b = other.b;
        break;
      case LISTTYPE:
        l = other.l;
        break;
      case FUNCTIONTYPE:
        f = other.f;
        break;
      case NILTYPE:
        break;
    }
    return *this;
  }


  friend std::ostream& operator<<(std::ostream& os, const Value& v) {
    switch (v.type) {
      case STRING:
        os << v.s;
        break;
      case INT:
        os << v.i;
        break;
      case DOUBLE:
        os << v.d;
        break;
      case BOOLTYPE:
        os << (v.b ? "true" : "false");
        break;
      case LISTTYPE:
        os << "[";
        for (auto& e : v.l) {
          os << e << ", ";
        }
        os << "]";
        break;
      case FUNCTIONTYPE:
        os << "function " << v.f.name;
        break;
      case NILTYPE:
        os << "nil";
        break;
    }
    return os;
  }

};

class Execution : public ast::Visitor {
 public:
  void visit(ast::Program* Function) override;
  void visit(ast::Import* Function) override;
  void visit(ast::Cond* Function) override;
  void visit(ast::Identifier* Function) override;
  void visit(ast::TypeIdentifier* Function) override;
  void visit(ast::FunctionCall* Function) override;
  void visit(ast::BuiltInFunctionCall* node) override;

  void visit(ast::LiteralInt* Function) override;
  void visit(ast::LiteralFloat* Function) override;
  void visit(ast::LiteralBool* Function) override;
  void visit(ast::LiteralList* Function) override;
  void visit(ast::LiteralNull* Function) override;
  void visit(ast::LiteralString* Function) override;

  void visit(ast::ReturnStatement* Function) override;
  void visit(ast::BreakStatement* Function) override;
  void visit(ast::WhileLoop* Function) override;

  void visit(ast::FunctionDecl* Function) override;
  void visit(ast::Lambda* Function) override;
  void visit(ast::VariableDecl* Function) override;
  void visit(ast::Variable* Function) override;

  void visit(ast::FunctionType* Function) override;
  void visit(ast::GenericTypeDecl* Function) override;
  void visit(ast::TypeAlias* Function) override;

  void visit(ast::IntType* Function) override;
  void visit(ast::FloatType* Function) override;
  void visit(ast::BoolType* Function) override;
  void visit(ast::ListType* Function) override;
  void visit(ast::NullType* Function) override;
  void visit(ast::StringType* Function) override;

 private:
  // for recording variables' values
  std::map<ast::Declaration*, Value> values;
  Value tempValue;
};
