#pragma once

#include <skiddo/buffer/buffer.hpp>

namespace buffer {

class InteractiveBuffer : public Buffer {
 public:
  virtual bool read_more();
};

}  // namespace buffer
