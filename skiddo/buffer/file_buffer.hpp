#pragma once

#include <skiddo/buffer/buffer.hpp>
#include <string>

namespace buffer {

class FileBuffer : public Buffer {
 private:
  std::string filename;
  FILE* file;

  FILE* get_file(const std::string& filename);

 public:
  explicit FileBuffer(const std::string& filename);
  virtual ~FileBuffer();

  virtual bool read_more();
};

}  // namespace buffer
