#pragma once

#include <skiddo/buffer/buffer.hpp>
#include <skiddo/parser/ast.hpp>
#include <skiddo/parser/tokenizer.hpp>

using namespace ast;

namespace parser {

class ASTBuilder {
 private:
  Tokenizer* tokenizer;
  TokenObject expect(const Token&, const std::string& err = "");
  bool nextIs(const Token& tokenType);

 public:
  explicit ASTBuilder(Tokenizer* tokenizer) : tokenizer(tokenizer) {}

  sPtr<Program> parse_prog();
  sPtr<Program> parse_program();
  sPtr<Import> parse_import();

  sPtr<Element> parse_element();
  sPtr<FunctionCall> parse_function_call();
  sPtr<Identifier> parse_identifier();
  sPtr<Cond> parse_cond();
  sPtr<BuiltInFunctionCall> parse_built_in_function();

  sPtr<LiteralInt> parse_int();
  sPtr<LiteralFloat> parse_float();
  sPtr<LiteralBool> parse_bool();
  sPtr<LiteralNull> parse_null();
  sPtr<LiteralList> parse_list();
  sPtr<LiteralString> parse_string();

  sPtr<Statement> parse_statement();
  sPtr<VariableDecl> parse_setq();
  sPtr<Variable> parse_reassign();
  sPtr<FunctionDecl> parse_function_decl();
  sPtr<Lambda> parse_lambda();
  sPtr<BreakStatement> parse_break();
  sPtr<ReturnStatement> parse_return();
  sPtr<WhileLoop> parse_while();
  sPtr<BaseNode> parse_any();
  sPtr<Type> parse_type();
  sPtr<TypeAlias> parse_type_def();
};

}  // namespace parser
