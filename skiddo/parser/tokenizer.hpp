#pragma once

#include <map>
#include <stack>

#include "../buffer/char_reader.hpp"
#include "token.hpp"

namespace parser {

class Tokenizer {
 private:
  buffer::BufferCharReader buf;

  int lineno; /* Current line number */

  std::stack<TokenObject> buffer;

 public:
  explicit Tokenizer(buffer::Buffer* buf);

  /**
   * @brief Get next token in buffer
   *
   * Returns a token, or ENDMARKER if there are no more tokens in the buffer
   *
   * @param start pointer to start of token
   * @param end pointer to end of token
   * @return token object
   */
  TokenObject get();
  TokenObject peek();
  void unget(const TokenObject&);
};

}  // namespace parser
