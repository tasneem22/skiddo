#pragma once

#include <cstdlib>
#include <map>
#include <string>

namespace parser {

enum Token {
  ENDMARKER = 0,         // No more tokens
  IDENTIFIER = 1,        // Identifier (most frequent token!)
  NIL = 2,               // null
  BOOL = 3,              // true or false
  INTEGER = 4,           // 42
  FLOAT = 5,             // 23.43
  LPAR = 6,              // (
  RPAR = 7,              // )
  QUOTE_SIGN = 8,        // Unevaluated literal
  COLON = 9,             // :
  LIST = 10,             // not used
  LAMBDA = 11,           //
  FUNCTION = 12,         //
  PROG = 13,             //
  SETQ_FUNCTION = 14,    //
  RETURN = 18,           //
  BREAK = 19,            //
  L_ANG_BRACKET = 20,    //
  R_ANG_BRACKET = 21,    //
  ARROW = 22,            //
  TRUE = 23,             //
  FALSE = 24,            //
  INT_TYPE = 25,         //
  FLOAT_TYPE = 26,       //
  BOOL_TYPE = 27,        //
  NULL_TYPE = 28,        //
  LIST_TYPE = 29,        //
  TYPE_DEF = 30,
  BUILT_IN_FUNCTION = 31,
  WHILE = 32, 
  CONDITION = 33,
  REASSIGN = 34,
  IMPORT = 35, //
};

extern const char* const TokenNames[]; /* Token names */

const std::map<std::string, Token> keywords_resolution = {
    {"setq", Token::SETQ_FUNCTION},
    {"func", Token::FUNCTION},
    {"lambda", Token::LAMBDA},
    {"prog", Token::PROG},
    {"return", Token::RETURN},
    {"break", Token::BREAK},
    {"cond", Token::CONDITION},
    {"while", Token::WHILE},
    {"true", Token::TRUE},
    {"false", Token::FALSE},
    {"assign", Token::REASSIGN},
    {"print", Token::BUILT_IN_FUNCTION},
    {"plus", Token::BUILT_IN_FUNCTION},
    {"minus", Token::BUILT_IN_FUNCTION},
    {"times", Token::BUILT_IN_FUNCTION},
    {"divide", Token::BUILT_IN_FUNCTION},
    {"head", Token::BUILT_IN_FUNCTION},
    {"tail", Token::BUILT_IN_FUNCTION},
    {"cons", Token::BUILT_IN_FUNCTION},
    {"equal", Token::BUILT_IN_FUNCTION},
    {"noneq", Token::BUILT_IN_FUNCTION},
    {"less", Token::BUILT_IN_FUNCTION},
    {"lesseq", Token::BUILT_IN_FUNCTION},
    {"greater", Token::BUILT_IN_FUNCTION},
    {"greatereq", Token::BUILT_IN_FUNCTION},
    {"isint", Token::BUILT_IN_FUNCTION},
    {"isreal", Token::BUILT_IN_FUNCTION},
    {"isbool", Token::BUILT_IN_FUNCTION},
    {"isnull", Token::BUILT_IN_FUNCTION},
    {"islist", Token::BUILT_IN_FUNCTION},
    {"and", Token::BUILT_IN_FUNCTION},
    {"or", Token::BUILT_IN_FUNCTION},
    {"xor", Token::BUILT_IN_FUNCTION},
    {"not", Token::BUILT_IN_FUNCTION},
    {"eval", Token::BUILT_IN_FUNCTION},
    {"int", Token::INT_TYPE},
    {"float", Token::FLOAT_TYPE},
    {"bool", Token::BOOL_TYPE},
    {"null", Token::NULL_TYPE},
    {"nil", Token::NIL},
    {"list", Token::LIST_TYPE},
    {"typeDef", Token::TYPE_DEF},
    {"import", Token::IMPORT}};

struct TokenObject {
  size_t start;
  size_t end;
  Token type;
  std::string value;
};

}  // namespace parser
