#pragma once

#include <memory>
#include <skiddo/buffer/buffer.hpp>
#include <skiddo/parser/tokenizer.hpp>
#include <utility>
#include <vector>
using namespace parser;
namespace ast {

template <typename T>
using sPtr = std::shared_ptr<T>;


struct BaseNode;
/**
 * element is either identifier, Literal or function call,
 * where any parameter in a function call is an element
 * evaluates to a value
 */
struct Element;
struct Program;
struct Cond;
/** a.k.a Atom */
struct Identifier;
struct TypeIdentifier;
/** a.k.a List */
struct FunctionCall;
struct Import;
struct Lambda;

struct Literal;
struct LiteralInt;
struct LiteralFloat;
struct LiteralBool;
struct LiteralNull;
struct LiteralList;
struct LiteralString;

struct Statement;
struct ReturnStatement;
struct WhileLoop;
struct BreakStatement;
struct BuiltInFunctionCall;

struct Declaration;
struct FunctionDecl;
struct VariableDecl;
struct Variable;

struct Type;
struct TypeDecl;
//struct StructDecl;
struct TypeAlias;
struct GenericTypeDecl;
struct ListType;
struct FunctionType;
struct PrimitiveType;

// the primitive types
struct BoolType;
struct NumberType;
struct IntType;
struct FloatType;
struct NullType;
struct StringType;

class Visitor {
 public:
  virtual ~Visitor() = default;

  virtual void visit(Program* Function) = 0;
  virtual void visit(Import* Function) = 0;
  virtual void visit(Cond* Function) = 0;
  virtual void visit(Identifier* Function) = 0;
  virtual void visit(TypeIdentifier* Function) = 0;
  virtual void visit(FunctionCall* Function) = 0;
  virtual void visit(BuiltInFunctionCall* Function) = 0;

  virtual void visit(LiteralInt* Function) = 0;
  virtual void visit(LiteralFloat* Function) = 0;
  virtual void visit(LiteralBool* Function) = 0;
  virtual void visit(LiteralList* Function) = 0;
  virtual void visit(LiteralNull* Function) = 0;
  virtual void visit(LiteralString* Function) = 0;

  virtual void visit(ReturnStatement* Function) = 0;
  virtual void visit(BreakStatement* Function) = 0;
  virtual void visit(WhileLoop* Function) = 0;

  virtual void visit(FunctionDecl* Function) = 0;
  virtual void visit(Lambda* Function) = 0;
  virtual void visit(VariableDecl* Function) = 0;
  virtual void visit(Variable* Function) = 0;

  //  virtual void visit(StructDecl* Function) = 0;
  virtual void visit(TypeAlias* Function) = 0;
  virtual void visit(GenericTypeDecl* Function) = 0;
  virtual void visit(FunctionType* Function) = 0;
  virtual void visit(ListType* Function) = 0;

  virtual void visit(IntType* Function) = 0;
  virtual void visit(FloatType* Function) = 0;
  virtual void visit(BoolType* Function) = 0;
  virtual void visit(NullType* Function) = 0;
  virtual void visit(StringType* Function) = 0;
};

struct BaseNode {
  // TODO: add position again for error reporting
  //        lexer::Token::Position begin, end;
  virtual void accept(Visitor& v) = 0;
  virtual ~BaseNode() = default;
};
struct Element : BaseNode {
  sPtr<Type> type;
  void accept(Visitor& v) override = 0;
};
struct Program : Element {
  std::vector<sPtr<BaseNode>> body;
  void accept(Visitor& v) override { v.visit(this); }
};

struct Import: Element {
  std::string path;
  explicit Import(std::string path) : path(path) {}
  void accept(Visitor& v) override { v.visit(this); }
};

struct Cond : Element {
  sPtr<Element> condition;
  sPtr<BaseNode> ifBody, elseBody;
  void accept(Visitor& v) override { v.visit(this); }
};
struct Identifier : Element {
  std::string name;
  sPtr<Declaration> declaration;
  explicit Identifier(std::string name) : name(std::move(name)) {}
  void accept(Visitor& v) override { v.visit(this); }
};

struct FunctionCall : Element {
  sPtr<Identifier> identifier;
  std::vector<sPtr<Type>> genericTypeArgs;
  std::vector<sPtr<Element>> arguments;
  void accept(Visitor& v) override { v.visit(this); }
};

struct BuiltInFunctionCall : Element {
  sPtr<Identifier> identifier;
  std::vector<sPtr<Element>> arguments;
  void accept(Visitor& v) override { v.visit(this); }
};

struct Statement : BaseNode {
  void accept(Visitor& v) override = 0;
};
struct WhileLoop : Statement {
  sPtr<Element> condition;
  sPtr<BaseNode> body;
  void accept(Visitor& v) override { v.visit(this); }
};
struct ReturnStatement : Statement {
  sPtr<Element> returnValue;
  void accept(Visitor& v) override { v.visit(this); }
};
struct BreakStatement : Statement {
  void accept(Visitor& v) override { v.visit(this); }
};


struct Type : BaseNode {
  void accept(Visitor& v) override = 0;
};
struct Declaration : Statement {
  std::string name;
  [[nodiscard]] virtual sPtr<Type> getType() const = 0;
  void accept(Visitor& v) override = 0;
};

struct PrimitiveType : Type {
  void accept(Visitor& v) override = 0;
};
struct NumberType : PrimitiveType {
  void accept(Visitor& v) override = 0;
};
struct FunctionType : Type {
  sPtr<Type> returnType, argType;
  void accept(Visitor& v) override { v.visit(this); };
};
// struct GenericType : Type {
//   std::vector<sPtr<TypeDecl>> typeParams;
//   void accept(Visitor& v) override { v.visit(this); };
// };

struct TypeDecl : Type {
  std::string name;
  void accept(Visitor& v) override = 0;
};
struct TypeIdentifier : Type {
  std::string name;
  sPtr<TypeDecl> declaration;
  void accept(Visitor& v) override { v.visit(this); }
};
//};
struct TypeAlias : TypeDecl {
  sPtr<Type> type;
  void accept(Visitor& v) override { v.visit(this); }
};
struct GenericTypeDecl : TypeDecl {
  sPtr<Type> type;
  void accept(Visitor& v) override { v.visit(this); }
};

// todo
// struct StructDecl : TypeDecl {
//  std::vector<sPtr<Identifier>> ;
//  void accept(Visitor& v) override{ v.visit(this); }

struct ListType : Type {
  sPtr<Type> elementType;
  void accept(Visitor& v) override { v.visit(this); }
};
struct IntType : NumberType {
  void accept(Visitor& v) override { v.visit(this); }
};

struct FloatType : NumberType {
  void accept(Visitor& v) override { v.visit(this); }
};
struct BoolType : PrimitiveType {
  void accept(Visitor& v) override { v.visit(this); }
};
struct NullType : PrimitiveType {
  void accept(Visitor& v) override { v.visit(this); }
};
struct StringType : PrimitiveType {
  void accept(Visitor& v) override { v.visit(this); }
};

struct Literal : Element {
  void accept(Visitor& v) override = 0;
};

struct LiteralFloat : Literal {
  double value;
  explicit LiteralFloat(double value) : value(value) { this->type = std::make_shared<FloatType>(); }
  void accept(Visitor& v) override { v.visit(this); }
};
struct LiteralBool : Literal {
  bool value;
  explicit LiteralBool(bool value) : value(value) { this->type = std::make_shared<BoolType>(); }
  void accept(Visitor& v) override { v.visit(this); }
};

struct LiteralNull : Literal {
  explicit LiteralNull() { this->type = std::make_shared<NullType>(); }
  void accept(Visitor& v) override { v.visit(this); }
};

struct LiteralString : Literal {
  std::string value;
  explicit LiteralString(std::string value) : value(std::move(value)) { this->type = std::make_shared<StringType>(); }
  void accept(Visitor& v) override { v.visit(this); }
};

// todo list, not done yet
struct LiteralList : Literal {
  std::vector<sPtr<Element>> elements;
  void accept(Visitor& v) override { v.visit(this); }
};

struct LiteralInt : Literal {
  uint64_t value;
  explicit LiteralInt(long long value) : value(value) { this->type = std::make_shared<IntType>(); }
  void accept(Visitor& v) override { v.visit(this); }
};

struct VariableDecl : Declaration {
  sPtr<Type> type;
  sPtr<Element> value;
  [[nodiscard]] sPtr<Type> getType() const override { return type; };
  void accept(Visitor& v) override { v.visit(this); }
};

struct Variable : VariableDecl {
  void accept(Visitor& v) override { v.visit(this); }
};

struct FunctionDecl : Declaration {
  sPtr<FunctionType> functionType;
  std::vector<sPtr<VariableDecl>> parameters;
  sPtr<BaseNode> body;
  std::vector<sPtr<GenericTypeDecl>> typeParams;
  [[nodiscard]] sPtr<Type> getType() const override { return functionType; };
  void accept(Visitor& v) override { v.visit(this); }
};

struct Lambda: FunctionDecl, Element {
  void accept(Visitor& v) override { v.visit(this); }
};

}  // namespace ast
