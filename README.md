# Skiddo programming language

This is a Advanced Compilers Construction and Program Analysis project, taught in Innopolis University for third-year bachelor students. The compiler is used to compile the Skiddo language, which is simply typed functional programming language inspired by Lisp.

You can read more about the language and its grammar [here](https://gitlab.com/tasneem22/skiddo/-/wikis/Language).



## Building the project

To build the **release** version of the project first install some dependencies:

```bash
apt-get install libreadline-dev
```

Build the project:

```bash
mkdir build && cd build
cmake .. && make --jobs=4
```

Now you can run the project:

```bash
./skiddo fileName.skd
```

## Running linter, tests and measure coverage

Run linter (you need to install [cpplint](https://github.com/cpplint/cpplint)):

```bash
./linter.sh
```

Run formatter (you need to install [clang-format](https://clang.llvm.org/docs/ClangFormat.html)):

```bash
./formatter.sh
```

Build project with tests and coverage support:

```bash
mkdir build && cd build
cmake -DBUILD_TESTS=ON -DCMAKE_BUILD_TYPE=Coverage .. && make -j$(nproc)
```

Run tests and coverage measurement (you need to install [gcovr](https://gcovr.com/en/stable/installation.html)):

```bash
make test && make coverage
```

## Released Version 

You can download the prebuild binaries from [here](https://gitlab.com/skiddo-cc-project/skiddo/-/jobs/artifacts/master/file/skiddo?job=build_prod) 


## 💻 Contributers

<p>

  :mortar_board: <i>All participants in this project are undergraduate students in the <a href="https://apply.innopolis.university/en/bachelor/">Department of Computer Science</a> <b>@</b> <a href="https://innopolis.university/">Innopolis University</a></i>. <br> <br>


  :boy: <b>Gleb Osotov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>g.osotov@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitHub: <a href="https://github.com/glebosotov">@glebosotov</a> <br>


  :girl: <b>Tasneem Toolba</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>t.samir@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitHub: <a href="https://github.com/tasneem22">@tasneem22</a> <br>

:girl: <b>Dariya Vakhitova </b> <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>d.vakhitova@innopolis.university</a> <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitHub: <a href="https://github.com/yadariya">@yadariya</a> <br>

:boy: <b>Dmitry Tsaplya</b> <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>d.tsaplya@innopolis.university</a> <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitHub: <a href="https://gitlab.com/tsaplyadmitriy">@tsaplyadmitriy</a> <br>
</p>

